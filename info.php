<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_loremGen
 * @author          LEPTON Project
 * @copyright       2013-2025 LEPTON Project
 * @link            http://www.lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$module_directory   = 'lib_loremgen';
$module_name        = 'LEPTON Lorem Gen(-erator)';
$module_function    = 'library';
$module_version     = '0.6.0.0';
$module_platform    = '7.1.0';
$module_delete      =  true;
$module_author      = 'LEPTON team, several independent authors for the lyrics.';
$module_license     = 'GNU General Public License';
$module_description = 'Class to generate some Lorem-Ipsum layout text.';
$module_home        = 'http://www.lepton-cms.org/';
$module_guid        = '864565EF-AA62-45E0-8EF5-C69F92393A7C';
