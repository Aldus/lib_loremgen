<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_loremGen
 * @author          LEPTON Project
 * @copyright       2013-2025 LEPTON Project
 * @link            http://www.lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

$MOD_LIB_LOREMGEN = [
    'hello' => "This is just another 'hello World' test."
];
